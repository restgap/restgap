# RestGap #
[![javadoc](https://javadoc.io/badge2/org.bitbucket.restgap/restgap/javadoc.svg)](https://javadoc.io/doc/org.bitbucket.restgap/restgap) 
![Maven-Central](https://img.shields.io/maven-central/v/org.bitbucket.restgap/restgap)

RestGap is a library which bridges the Gap between JAX-RS annotated interfaces and Spring's RestTemplate. When Using RestGap, you provide a JAX-RS annotated Java interface to the RestGap factory, along with a RestTemplate instance, and you'll get a HTTP client object that implements your JAX-RS interface.

Other than that, zero coding needed.

## Usage ##

[See the Wiki](https://bitbucket.org/restgap/restgap/wiki/Home) for details on how to use REST Gap.

## Requirements / Support ##

From Version 0.7 onwards, RestGap requires Java 17, Spring 6 and/or JAX RS in
Jakarta EE (`jakarta.ws.rs.*`).

RestGap up until version 0.6 supports Java 8 and Spring 4+ and/or Java EE
(`javax.ws.rs.*`).

## Maven Depencency ##
``` xml
<depencency>
    <groupId>org.bitbucket.restgap</groupId>
    <artifactId>restgap</artifactId>
    <version>${version}</version> <!-- see maven-central shield above for the ${version} to use -->
</dependency>
```