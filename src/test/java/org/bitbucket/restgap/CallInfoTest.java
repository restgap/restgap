/*
 * Copyright 2020 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.net.URI;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import org.bitbucket.restgap.frontend.jaxrs.JAXRSFrontend;
import org.bitbucket.restgap.frontend.springmvc.SpringMVCFrontend;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author upachler
 */
public class CallInfoTest {
	
	interface Prompter {
		String sayHello(@QueryParam("name") String name); 
		void bye(String reason);
		void getFile(@PathParam("filename") String filename);
	}
	
	@Path("/prompter")
	interface JAXRSPrompter extends Prompter{
		@Override
		@Path("/hello")
		@GET
		String sayHello(@QueryParam("name") String name); 
		
		@Override
		@Path("/bye")
		@POST
		void bye(String reason);
		
		@Override
		@Path("/path/to/{filename}")
		@GET
		void getFile(@PathParam("filename") String filename);
	}
	
	@RequestMapping(path = "/prompter")
	interface SpringMVCPrompter extends Prompter{
		@Override
		@RequestMapping(path = "/hello",method = RequestMethod.GET)
		String sayHello(@RequestParam("name") String name); 
		
		@Override
		@RequestMapping(path = "/bye",method = RequestMethod.POST)
		void bye(@RequestBody String reason);
		
		@Override
		@RequestMapping(path="/path/to/{filename}", method = RequestMethod.GET)
		void getFile(@PathVariable("filename") String filename);
	}
	
	@Test
	public void testInfoOfJAXRS() {
		ClientFactory<JAXRSPrompter> cf = ClientFactory.ofInterface(JAXRSPrompter.class)
			.baseUri("/")
			.frontend(JAXRSFrontend.instance())
			.nullBackend()
			.build();
		testInfoOf(cf);
	}
	
	@Test
	public void testInfoOfSpringMVC() {
		ClientFactory<SpringMVCPrompter> cf = ClientFactory.ofInterface(SpringMVCPrompter.class)
			.baseUri("/")
			.frontend(SpringMVCFrontend.instance())
			.nullBackend()
			.build();
		testInfoOf(cf);
	}
	
	private void testInfoOf(ClientFactory<? extends Prompter> cf) {
		
		CallInfo fileCallInfo = cf.infoOf(p -> p.getFile("icon.png"));
		assertEquals(URI.create("/prompter/path/to/icon.png"), fileCallInfo.uri());
		assertEquals("GET", fileCallInfo.method());
		
		CallInfo helloCallInfo = cf.infoOf(p -> p.sayHello("John"));
		assertEquals(URI.create("/prompter/hello?name=John"), helloCallInfo.uri());
		assertEquals("GET", helloCallInfo.method());
		
		CallInfo byeCallInfo = cf.infoOf(p -> p.bye("cee ya"));
		assertEquals(URI.create("/prompter/bye"), byeCallInfo.uri());
		assertEquals("POST", byeCallInfo.method());
		
	}
}
