/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import org.bitbucket.restgap.backend.spring.RestTemplateBackend;
import org.bitbucket.restgap.frontend.jaxrs.JAXRSFrontend;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author upachler
 */
public class BuilderTest {
	void test() {
	
		ClientFactory<MyClient> clientFactory = ClientFactory
			.ofInterface(MyClient.class)
			.baseUri("/")	// NOTE: this won't really work to make
				// actual calls, because this URI is not absolute. However, it
				// /will/ work for creating relative URIs with absolute paths
			.frontend(JAXRSFrontend.instance())
			.backend(RestTemplateBackend.instance())
			.build();

		clientFactory.create();
	}
	
	
}

interface MyClient {
	
	void postStuff();
}
