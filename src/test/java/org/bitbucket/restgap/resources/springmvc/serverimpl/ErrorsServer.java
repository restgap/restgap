/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.springmvc.serverimpl;

import org.bitbucket.restgap.resources.springmvc.Errors;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author upachler
 */
@Controller
public class ErrorsServer implements Errors{
	
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	static public class ResourceNotFoundException extends RuntimeException {
		
	}
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	static public class InternalServerErrorException extends RuntimeException {
		
	}
	@Override
	public void error404() {
		throw new ResourceNotFoundException();
	}

	@Override
	public void error500() {
		throw new InternalServerErrorException();
	}
	
}
