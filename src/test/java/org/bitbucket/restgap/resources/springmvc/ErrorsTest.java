/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.springmvc;


import jakarta.servlet.Servlet;
import org.bitbucket.restgap.JettyTestUtil;
import static org.bitbucket.restgap.JettyTestUtil.shutdownJetty;
import static org.bitbucket.restgap.JettyTestUtil.startupJetty;
import org.bitbucket.restgap.RESTTemplateSpringMVCFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import static org.junit.Assert.assertEquals;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

/**
 *
 * @author count
 */
public class ErrorsTest {
	static JettyTestUtil.JettySetup setup;
	
	@BeforeClass
	public static void startup() throws Exception {
		setup = startupJetty("WEB-INF/applicationContext-springmvc-errorsTest.xml", new JettyTestUtil.ServletFactory() {
			@Override
			public Servlet createServlet(WebApplicationContext ctx) {
				return new DispatcherServlet(ctx);
			}
		});
	}
	
	@AfterClass
	public static void shutdown() throws Exception {
		shutdownJetty(setup);
	}
	
	private RestTemplate rt() {
		return new RestTemplate();
	}
	
	private String baseUri() {
		return setup.getBaseUri().toASCIIString();
	}
	
	@Test
	public void test404() {
		try {
			Errors errors = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), Errors.class);
			errors.error404();
		} catch(HttpClientErrorException hcex) {
			assertEquals(404, hcex.getStatusCode().value());
		}
	}
	
	@Test
	public void test500() {
		try {
			Errors errors = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), Errors.class);
			errors.error500();
		} catch(HttpServerErrorException hsex) {
			assertEquals(500, hsex.getStatusCode().value());
		}
	}
}
