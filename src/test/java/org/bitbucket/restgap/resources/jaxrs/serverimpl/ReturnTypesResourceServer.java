/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs.serverimpl;

import jakarta.ws.rs.core.Response;
import org.bitbucket.restgap.resources.jaxrs.CustomBean;
import org.bitbucket.restgap.resources.jaxrs.ReturnTypesResource;

/**
 *
 * @author upachler
 */
public class ReturnTypesResourceServer implements ReturnTypesResource{
	
	
	@Override
	public void getVoid() {
	}

	@Override
	public Response getResponse() {
		return Response.ok().build();
	}

	@Override
	public Integer getInt() {
		return 42;
	}

	@Override
	public CustomBean getCustomBeam() {
		CustomBean cb = new CustomBean();
		cb.setBar(42);
		cb.setFoo("foo");
		return cb;
	}
	
}
