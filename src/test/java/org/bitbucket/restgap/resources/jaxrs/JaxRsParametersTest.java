/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs;

import org.bitbucket.restgap.*;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import jakarta.servlet.Servlet;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;
import org.apache.cxf.transport.servlet.CXFServlet;
import static org.bitbucket.restgap.JettyTestUtil.shutdownJetty;
import static org.bitbucket.restgap.JettyTestUtil.startupJetty;
import org.bitbucket.restgap.backend.jaxrs.JAXRSBackend;
import org.bitbucket.restgap.backend.spring.RestTemplateBackend;
import org.bitbucket.restgap.client.RestCallContext;
import org.bitbucket.restgap.client.ResultHandler;
import org.bitbucket.restgap.frontend.jaxrs.JAXRSFrontend;
import org.bitbucket.restgap.resources.jaxrs.serverimpl.AuthenticatedResourceServer;
import org.bitbucket.restgap.resources.jaxrs.serverimpl.HeaderParametersResourceServer;
import org.bitbucket.restgap.resources.jaxrs.serverimpl.PathParametersResourceServer;
import org.bitbucket.restgap.resources.jaxrs.serverimpl.QueryParametersResourceServer;
import org.bitbucket.restgap.resources.jaxrs.serverimpl.ReturnTypesResourceServer;
import org.junit.Test;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.web.client.RestTemplate;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.http.HttpMethod;
import org.springframework.web.context.WebApplicationContext;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author count
 */
public class JaxRsParametersTest {
	
	private static final URI BASE_URI = URI.create("http://testhost/");
	private static JettyTestUtil.JettySetup setup;
	
	private static PathParametersResourceServer pathParamsServer;
	private static QueryParametersResourceServer queryParamsServer;
	private static HeaderParametersResourceServer headerParamsServer;
	private static AuthenticatedResourceServer authenticatedResourceServer;
	private static ReturnTypesResourceServer returnTypesResourceServer;
	private static Client client;
	
	@BeforeClass
	public static void startup() throws Exception {
		client = ClientBuilder.newClient();

		setup = startupJetty("WEB-INF/applicationContext-jaxrs-parametersTest.xml", new JettyTestUtil.ServletFactory() {
			@Override
			public Servlet createServlet(WebApplicationContext ctx) {
				return new CXFServlet();
			}
		});
		pathParamsServer = setup.getSpringContext().getBean(PathParametersResourceServer.class);
		queryParamsServer = setup.getSpringContext().getBean(QueryParametersResourceServer.class);
		headerParamsServer = setup.getSpringContext().getBean(HeaderParametersResourceServer.class);
		authenticatedResourceServer = setup.getSpringContext().getBean(AuthenticatedResourceServer.class);
		returnTypesResourceServer = setup.getSpringContext().getBean(ReturnTypesResourceServer.class);
	}
	
	@AfterClass
	public static void shutdown() throws Exception {
		shutdownJetty(setup);
		client.close();
	}
	
	public JaxRsParametersTest() {
	}
	
	private RequestMatcher requestPath(String path) {
		return requestTo(BASE_URI.resolve(path));
	}
	
	/**
	 * Testing query parameters
	 */
	@Test
	public void testQueryParameters() {
		testQueryParametersImpl(mkInstanceJaxRs(QueryParametersResource.class));
		testQueryParametersImpl(mkInstanceRestTemplate(QueryParametersResource.class));
	}
	
	private ResultHandler AUTH_RESULT_HANDLER = new ResultHandler() {
		@Override
		public CompletableFuture<RestCallContext> handleResult(RestCallContext context) {
			if(context.getResult().getStatusCode() == 401) {
				String auth = acquireAuthentication();
				context.getCall().getHeaders().put("Authorization", Collections.singletonList("Basic " + auth));
				context.reissue();
			}
			return CompletableFuture.completedFuture(context);
		}
	};
	
	private <T> T mkInstanceJaxRs(Class<T> clazz) {
		ClientFactory<T> f = ClientFactory.ofInterface(clazz)
			.baseUri(setup.getBaseUri().toASCIIString())
			.frontend(JAXRSFrontend.instance())
			.backend(RestTemplateBackend.instance())
			.build();
		
		T proxy = f.create();
		org.bitbucket.restgap.client.RestGapClient c = org.bitbucket.restgap.client.RestGapClient.getClient(proxy);
		
		c.addResultHandler(AUTH_RESULT_HANDLER);
		
		return proxy;
	}
	
	private String acquireAuthentication() {
		String credential = AuthenticatedResourceServer.USERNAME + ":" + AuthenticatedResourceServer.PASSWORD;
		return Base64.getEncoder().encodeToString(credential.getBytes(StandardCharsets.UTF_8));
	}
		
	private <T> T mkInstanceRestTemplate(Class<T> clazz) {
		
		ClientFactory<T> f = ClientFactory.ofInterface(clazz)
			.baseUri(setup.getBaseUri().toASCIIString())
			.frontend(JAXRSFrontend.instance())
			.backend(JAXRSBackend.instance())
			.build();
		
		T proxy = f.create();
		org.bitbucket.restgap.client.RestGapClient c = org.bitbucket.restgap.client.RestGapClient.getClient(proxy);
		
		c.addResultHandler(AUTH_RESULT_HANDLER);
		
		return proxy;
	}
	
	private void testQueryParametersImpl(QueryParametersResource instance) {
		
		instance.queryInt(42);
		assertEquals(42, queryParamsServer.getLastQueryInt());
		
		instance.queryFloat(42.5f);
		assertEquals(42.5f, queryParamsServer.getLastQueryFloat(), 0.0001f);
		
		instance.queryBoolean(true);
		assertEquals(true, queryParamsServer.getLastQueryBoolean());
		
		instance.queryBoolean(false);
		assertEquals(false, queryParamsServer.getLastQueryBoolean());
		
		instance.queryStrings(Arrays.asList("foo", "bar"));
		assertEquals(Arrays.asList("foo", "bar"), queryParamsServer.getLastQueryStrings());
		
		// check encoding for query parameter separator characters '?' and '&',
		// as well as unicode characters alpha, beta, and gamma
		String QESTION_AMP_ALPHA_BETA_GAMMA = "?&\u03b1\u03b2\u03b3";
		instance.queryStrings(Arrays.asList("foo", QESTION_AMP_ALPHA_BETA_GAMMA));
		assertEquals(Arrays.asList("foo", QESTION_AMP_ALPHA_BETA_GAMMA), queryParamsServer.getLastQueryStrings());
		
		instance.queryStrings(null);
		assertTrue(queryParamsServer.getLastQueryStrings() == null || queryParamsServer.getLastQueryStrings().isEmpty());
	}

	/**
	 * Testing path parameters
	 */
	@Test
	public void testPathParameters() {
		testPathParametersImpl(mkInstanceJaxRs(PathParametersResource.class));
		testPathParametersImpl(mkInstanceRestTemplate(PathParametersResource.class));
		
	}
	
	public void testPathParametersImpl(PathParametersResource instance) {
		
		instance.getPathInt(42);
		
		assertEquals(42, pathParamsServer.getLastPathInt());
	}
	
	@Test
	public void testHeaderParameters() {
		testHeaderParametersImpl(mkInstanceJaxRs(HeaderParametersResource.class));
		testHeaderParametersImpl(mkInstanceRestTemplate(HeaderParametersResource.class));
	}
	
	public void testHeaderParametersImpl(HeaderParametersResource instance) {
		
		instance.postHeaderParams("string!", 42, 42.5f);
		
		assertEquals("string!", headerParamsServer.getLastStringParam());
		assertEquals(42, headerParamsServer.getLastIntParam());
		assertEquals(42.5f, headerParamsServer.getLastFloatParam(), 0.0001f);
	}
	
	@Test
	public void testAuthChallenge() {
		
		testAuthChallengeImpl(mkInstanceJaxRs(AuthenticatedResource.class));
		testAuthChallengeImpl(mkInstanceRestTemplate(AuthenticatedResource.class));
		
	}
	
	private void testAuthChallengeImpl(AuthenticatedResource instance) {
		instance.post();
		assertTrue(authenticatedResourceServer.lastCallSuccessful());
	}
	
	@Test
	public void testAuthorization() throws UnsupportedEncodingException {
		
		// we'e testing authorization with MockRestServiceServer, as it is quite a chore to set
		// that up; maybe another time
		final String username = "Aladdin";
		final String password = "open sesame";
		
		String auth = "Basic " + Base64.getEncoder().encodeToString((username + ':' + password).getBytes("ISO-8859-1"));
		RestTemplate rt = rt();
		MockRestServiceServer mrss = mrss(rt);
		
		NonParameterizedResource instance = RESTTemplateJAXRSClientFactory.create(rt, BASE_URI.toString(), NonParameterizedResource.class, username, password);
		
		mrss
			.expect(requestPath("/no-params"))
			.andExpect(method(HttpMethod.POST))
			.andExpect(header("Authorization", auth))
			.andRespond(withSuccess());
			;
		
		instance.post();
		
		mrss.verify();
	}

	@Test
	public void testReturnTypes() {
		// NOTE: this test only works for the RestGap JAX RS Backend, because
		// the return type of this entity is javax.ws.rs.core.Response, which
		// we can't provide easily with RestTemplate
		testReturnTypesImpl(mkInstanceRestTemplate(ReturnTypesResource.class));
	}
	
	private void testReturnTypesImpl(ReturnTypesResource instance) {
		
		instance.getVoid();
		
		Response r = instance.getResponse();
		assertEquals(200, r.getStatus());
		r.close();
		
/*		Integer i = instance.getInt();
		assertEquals((Integer)42, i);
		
		CustomBean cb = instance.getCustomBeam();
		assertEquals(42, cb.getBar());
		assertEquals("foo", cb.getFoo());
*/
	}
	
	private RestTemplate rt() {
		return new RestTemplate();
	}
	
	private MockRestServiceServer mrss(RestTemplate rt) {
		MockRestServiceServer mrss = MockRestServiceServer.createServer(rt);
		return mrss;
	}
	
}
