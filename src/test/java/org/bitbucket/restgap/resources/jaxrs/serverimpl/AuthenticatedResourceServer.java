/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs.serverimpl;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import org.bitbucket.restgap.resources.jaxrs.AuthenticatedResource;

/**
 *
 * @author upachler
 */
public class AuthenticatedResourceServer implements AuthenticatedResource{

	public static String USERNAME = "john_doe";
	public static String PASSWORD = "s3cr5t";
	
	@Context
	HttpServletRequest hreq;
	private boolean lastCallSuccessful;
	
	private boolean isAuthenticated(String auth) {
		final String BASIC = "basic";
		String[] authComponents = auth.split("\\s+", 2);
		if(authComponents.length<2 || !authComponents[0].equalsIgnoreCase(BASIC)) {
			return false;
		}
		
		String userPass = new String(Base64.getDecoder().decode(authComponents[1]), StandardCharsets.UTF_8);
		String[] userPassComponents = userPass.split(":", 2);
		if(userPassComponents.length < 2) {
			return false;
		}
		
		return userPassComponents[0].equals(USERNAME) && userPassComponents[1].equals(PASSWORD);
	}
	
	@Override
	public void post() {
		String auth = hreq.getHeader("Authorization");
		if(auth == null || !isAuthenticated(auth)) {
			lastCallSuccessful = false;
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		lastCallSuccessful = true;
	}

	public boolean lastCallSuccessful() {
		return lastCallSuccessful;
	}
	
}
