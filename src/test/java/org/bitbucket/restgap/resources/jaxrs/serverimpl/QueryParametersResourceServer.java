/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs.serverimpl;

import java.util.List;
import org.bitbucket.restgap.resources.jaxrs.QueryParametersResource;

/**
 *
 * @author upachler
 */
public class QueryParametersResourceServer implements QueryParametersResource {

	private int lastQueryInt;
	private float lastQueryFloat;
	private boolean lastQueryBoolean;
	private List<String> lastQueryStrings;

	@Override
	public void queryInt(int value) {
		lastQueryInt = value;
	}

	@Override
	public void queryFloat(float value) {
		lastQueryFloat = value;
	}

	@Override
	public void queryBoolean(boolean value) {
		lastQueryBoolean = value;
	}

	@Override
	public void queryStrings(List<String> value) {
		lastQueryStrings = value;
	}

	public int getLastQueryInt() {
		return lastQueryInt;
	}

	public float getLastQueryFloat() {
		return lastQueryFloat;
	}

	public boolean getLastQueryBoolean() {
		return lastQueryBoolean;
	}

	public List<String> getLastQueryStrings() {
		return lastQueryStrings;
	}
	
	
}
