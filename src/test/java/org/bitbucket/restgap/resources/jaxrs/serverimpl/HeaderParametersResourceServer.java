/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs.serverimpl;

import org.bitbucket.restgap.resources.jaxrs.HeaderParametersResource;

/**
 *
 * @author upachler
 */
public class HeaderParametersResourceServer implements HeaderParametersResource {

	private String lastStringParam;
	private int lastIntParam;
	private float lastFloatParam;

	@Override
	public void postHeaderParams(String stringParam, int intParam, float floatParam) {
		this.lastStringParam = stringParam;
		this.lastIntParam = intParam;
		this.lastFloatParam = floatParam;
	}

	public String getLastStringParam() {
		return lastStringParam;
	}

	public int getLastIntParam() {
		return lastIntParam;
	}

	public float getLastFloatParam() {
		return lastFloatParam;
	}
}
	

