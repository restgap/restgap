/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import org.bitbucket.restgap.client.CallHandler;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.client.RestGapClient;
import org.bitbucket.restgap.resources.springmvc.OpenWeatherMap2;
import org.bitbucket.restgap.resources.springmvc.OpenWeatherMap3;
import org.bitbucket.restgap.resources.springmvc.OpenWeatherMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import org.bitbucket.restgap.client.RestCallContext;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author count
 */
public class RESTTemplateSpringMVCFactoryTest {
	private static final String APPID = "71d7f2afcffbc509a149b53e3abebc9b";
	
	private final CallHandler appIdCallHandler = new CallHandler() {
		@Override
		public CompletionStage<RestCallContext> handleCall(RestCallContext ctx) {
			// add query param 'appid' to every call
			RestCall rc = ctx.getCall();
			rc.setUri(UriComponentsBuilder.fromHttpUrl(rc.getUri()).queryParam("appid", APPID).toUriString());
			return CompletableFuture.completedFuture(ctx);
		}
	};
	



	public RESTTemplateSpringMVCFactoryTest() {
	}

	/**
	 * Test of create method, of class RESTTemplateSpringMVCFactory.
	 */
	@Test
	public void testCreate() {
		System.out.println("create");
		OpenWeatherMap result = RESTTemplateSpringMVCFactory.create(rt(), "http://api.openweathermap.org", OpenWeatherMap.class);
		Map<String,Object> m = result.weatherByCityName(APPID, "London,uk");
		
		System.out.println(m.toString());
	}
	
	@Test
	public void testCreate2() {
		System.out.println("create");
		OpenWeatherMap2 result = RESTTemplateSpringMVCFactory.create(rt(), "http://api.openweathermap.org", OpenWeatherMap2.class);
		RestGapClient.getClient(result).addCallHandler(appIdCallHandler);
		Map<String,Object> m = result.weatherByCityName("London,uk");
		
		System.out.println(m.toString());
	}
	
	@Test
	public void testCreate3() {
		System.out.println("create");
		OpenWeatherMap3 result = RESTTemplateSpringMVCFactory.create(rt(), "http://api.openweathermap.org", OpenWeatherMap3.class);
		RestGapClient.getClient(result).addCallHandler(appIdCallHandler);
		ResponseEntity<Map<String,Object>> e = result.weatherByCityName("London,uk");
		Map<String,Object> m = e.getBody();
		
		System.out.println(m.toString());
	}
	
	private RestTemplate rt() {
		return new RestTemplate();
	}
	
}
