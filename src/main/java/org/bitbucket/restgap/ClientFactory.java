/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import org.bitbucket.restgap.impl.CallInfoHarvester;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.function.Consumer;
import org.bitbucket.restgap.backend.Backend;
import org.bitbucket.restgap.backend.PlanExecutor;
import org.bitbucket.restgap.backend.jaxrs.JAXRSBackend;
import org.bitbucket.restgap.backend.spring.RestTemplateBackend;
import org.bitbucket.restgap.client.CallHandler;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.frontend.jaxrs.JAXRSFrontend;
import org.bitbucket.restgap.frontend.springmvc.SpringMVCFrontend;

/**
 * <p>Creates RestGap proxy clients for REST resource interfaces with 
 * annotations from popular frameworks like JAX RS or Spring MVC.</p>
 * 
 * <p>Features:
 * <ul>
 * <li>Create REST clients from resource interfaces only, by interpreting annotations on 
 * interface and methods</li>
 * <li>Create call URIs for service methods by simply calling them</li>
 * </ul>
 * </p>
 * 
 * <p>A resource interface is a Java interface with JAX RS or Spring MVC annotations,
 * like this one:</p>
 * 
 * <pre> 
 * &#64;Path("/hello")
 * public interface Prompter {
 *     &#64;GET
 *     &#64;Produces("text/plain")
 *     String sayHello(&#64;QueryParam("name") String name);
 * }
 * </pre>
 * 
 * <h2>Create a client</h2>
 * 
 * <p>To create a client, a {@code ClientFactory} is required first, which is then used
 * to create clients quickly later. On creation, the factory performs an analysis on the resource
 * interface to produce call plans, which are then used by
 * the client instances that it produces.</p>
 * 
 * A client factory is assembled from two components:
 * <ul>
 * <li>A {@link Frontend}, which scans the given resource interface for 
 * annotated resource methods. RestGap supports JAX RS and Spring MVC annotations,
 * and provides frontends for each of them ({@link JAXRSFrontend} and 
 * {@link SpringMVCFrontend}.</li>
 * <li>To be able to make actual REST calls, RestGap also relies on popular
 * frameworks, for which backend implementations exist ({@link RestTemplateBackend}
 * and {@link JAXRSBackend}).</li>
 * </ul>
 * 
 * <p>For instance, to produce a client factory to produce clients for our 
 * {@code Prompter} resource interface, start by calling 
 * {@link ClientFactory#ofInterface(java.lang.Class) }, and then fill in the
 * builder methods, like this:</p>
 * 
 * <pre>{@code
 * ClientFactory<Prompter> clientFactory = ClientFactory
 *    .ofInterface(Prompter.class)
 *    .frontend(JAXRSFrontend.instance())
 *    .backend(RestTemplateBackend.instance())
 *    .build();
 * }</pre>
 * 
 * <p>To actually produce a client, call the {@link #create} method on the new factory.
 * However, the backend that we used still requires an actual instance of the
 * low-level client that it uses. That low-level client needs to be supplied
 * to the {@link ClientFactory#create(java.lang.Object) } method, like this:</p>
 * 
 * <pre>{@code 
 * RestTemplate restTemplate = ...; // create a new one, or use a global one that is pre-configured		
 *  
 * Prompter cl = clientFactory.create(restTemplate);
 * 
 * // now call sayHello()
 * String message = cl.sayHello("Dave");
 * 
 * // if the server returns 'Hello, Dave', that's what message will contain.
 * }</pre>
 * 
 * <p>The client actually behaves like a server implementation of the interface:
 * When you call {@code Prompter.sayHello()}, the framework will interpret
 * the response and serve it as a return value of the call. This works with
 * simple types (like {@code String}) as well as with complex classes, as long
 * as the underlying backend is able to map them to the incoming response of 
 * the server.</p>
 * 
 * <p>Should the server return an error code, like {@code 400} or {@code 404}, 
 * the framework's respective exception will be thrown (for instance, for JAX RS
 * these are {@code BadRequestException} or {@code NotFoundException}</p>
 * 
 * 
 * 
 * 
 * <h2>Create URLs for services</h2>
 * 
 * <p>While RestGap is mainly intended to make actual REST service calls, it is
 * also possible to get information about which REST call <i>would be made</i> without
 * actually calling the serivce. This is useful for servers that need to supply
 * URIs of their services to clients, with actual parameters filled in.
 * Use the {@link #infoOf} method for this to obtain a {@link CallInfo} instance.</p>
 * 
 * For a resource interface like this...
 * <pre> 
 * &#64;Path("/hello")
 * public interface Prompter {
 *     &#64;GET
 *     &#64;Produces("text/plain")
 *     String sayHello(&#64;QueryParam("name") String name);
 * }
 * </pre>
 * 
 * ..harvesting call information works like so:
 * <pre>{@code 
 * ClientFactory<Prompter> cf = ...;	// a client factory
 * 
 * // call a service method in a lambda expression on a resource provided
 * // by the factory
 * URI uri = cf.infoOf(p -> p.sayHello("John")).uri();
 * 
 * // the URI produced here is "/hello?name=John" (assuming the baseUri for
 * // the client factory is '/')
 * }</pre>
 * 
 * <h2>Under the hood</h2>
 * 
 * When creating a new {@code ClientFactory}, RestGap analyses the annotations on
 * the interface and produces call plans for the resource methods on that 
 * interface. To create an instance of a resource interface, RestGap uses 
 * <code><a href='https://docs.oracle.com/javase/8/docs/api/java/lang/reflect/Proxy.html'>java.lang.reflect.Proxy</a></code>
 * to create a proxy instance, with a call handler that is called whenever 
 * <i>any</i> method on that interface is called. From there it's easy: 
 * RestGap selects the call plan for that method, picks the parameters and
 * uses the backend to make the actual REST call.
 * @author upachler
 */
public class ClientFactory<T> {

	private final String baseUri;
	private final Class<T> clazz;
	private final Frontend frontend;
	private final Backend backend;
	private final Map<String,Plan> plans;
	private final ClassLoader cl;
	private transient CallInfoHarvester<T> callInfoHarvester;

	
	
	/**
	 * A builder used to create a client factory. Represents the first steps
	 * of building the factory.
	 * @param <T> the resource interface type
	 */
	public interface Builder1<T> {
		/**
		 * The base URI that should be used. All resource paths will be resolved
		 * against this base URI. This should be an absolute URI so that calls
		 * can be made correctly.
		 * @param baseUri	the base URI to use for calls.
		 * @return a partial builder
		 */
		Builder2<T> baseUri(String baseUri);
	}
	
	public interface Builder2<T> {
		/**
		 * The call frontent technology to use. Choose the fontend implementation
		 * depending on the annotation library you used on your resource interfaces.
		 * Spring MVC and JAX RS use different annotations; use the frontend
		 * matching them.
		 * @see SpringMVCFrontend
		 * @see JAXRSFrontend
		 * @param frontend	fontend to use for parsing resource annotations
		 * @return a partial builder
		 */
		Builder3<T> frontend(Frontend frontend);
	}
	
	public interface Builder3<T> {
		/**
		 * Choose the backend to use for the client. A Backend makes the actual
		 * REST calls, and wraps techologies such as Spring RestTemplate or
		 * the JAX RS Client API. Provide a backend instance to use for the
		 * client.
		 * @see JAXRSBackend#instance()
		 * @see RestTemplateBackend#instance() 
		 * @param <F> the frontend type that the backend will use
		 * @param backend	the {@link Backend} implementation to use
		 * @return completed builder
		 */
		<F> CompleteBuilder<T> backend(Backend backend);
		
		/**
		 * Choose to not set a Backend implementation. This will prevent the 
		 * factory from creating actual clients by calling {@link ClientFactory#create },
		 * but it will still be able to produce call info with the
		 * {@link ClientFactory#infoOf }. This is useful for
		 * server implementations that don't need to make REST calls, but often
		 * must provide full URIs to the services they implement.
		 * @return completed builder
		 */
		CompleteBuilder<T> nullBackend();
		
	}
	
	public interface CompleteBuilder<T> {
		ClientFactory<T> build();
	}
	
	static class BuilderImpl<T> implements Builder1<T>, Builder2<T>, Builder3<T> {
		Class<T> clazz;
		Frontend frontend;
		String baseUri;

		public BuilderImpl(Class<T> clazz) {
			this.clazz = clazz;
		}
		
		@Override
		public Builder2<T> baseUri(String baseUri) {
			this.baseUri = baseUri;
			return this;
		}

		@Override
		public Builder3<T> frontend(Frontend frontend) {
			this.frontend = frontend;
			return this;
		}

		@Override
		public <F> CompleteBuilder<T> backend(final Backend backend) {
			if(backend == null) {
				throw new NullPointerException("provided backend must be non-null");
			}
			return new CompleteBuilder<T>() {
				@Override
				public ClientFactory<T> build() {
					return ClientFactory.<T>build(BuilderImpl.this, backend);
				}
			};
		}
		@Override
		public CompleteBuilder<T> nullBackend() {
			return new CompleteBuilder<T>() {
				@Override
				public ClientFactory<T> build() {
					return ClientFactory.<T>build(BuilderImpl.this, null);
				}
			};
		}
		
	}
	
	public static <T> Builder1<T> ofInterface(Class<T> clazz) {
		return new BuilderImpl(clazz);
	}
	
	private static <T> ClientFactory<T> build(BuilderImpl<T> b, final Backend backend) {
		
		final Class clazz = b.clazz;
		final ClassLoader cl = clazz.getClassLoader();

		return new ClientFactory<T>(clazz, b.baseUri, b.frontend, backend, cl);
	}
	
	private ClientFactory(Class<T> clazz, String baseUri, Frontend frontend, Backend backend, ClassLoader cl) {
		this.clazz = clazz;
		this.baseUri = baseUri;
		this.plans = frontend.createPlans(clazz, baseUri);
		this.cl = cl;
		this.frontend = frontend;
		this.backend = backend;
	}
	
	/**
	 * Create a new client for the factory's resource interface
	 * @return a new REST client
	 */
	public T create() {
		return create(null, null);
	}
	
	/**
	 * Create a new client for the factory's resource interface, using the
	 * provide HTTP basic auth credentials. This is a convenience method for
	 * HTTP Basic Authentication. To add credential information for other 
	 * authentication schemes, consider adding a {@link CallHandler} to the
	 * client using 
	 * @param username	username for HTTP Basic auth
	 * @param password	password for HTTP Basic auth
	 * @return a new REST client
	 */
	public T create(String username, String password) {
		if(backend == null) {
			throw new IllegalStateException("the NULL backend was set, therefore no actual clients can be created.");
		}
		PlanExecutor executor = backend.createPlanExecutor();
		return createForExecutor(executor, username, password);
	}
	
	private T createForExecutor(PlanExecutor executor, String username, String password) {
		PlanInvocationHandler handler = new PlanInvocationHandler(clazz, executor, plans);
		if(username!=null || password!=null) {
			handler.addCallHandler(new BasicAuthCallHandler(username, password));
		}
		return clazz.cast(Proxy.newProxyInstance(cl, new Class[]{clazz}, handler));
	}
	
	private CallInfoHarvester<T> createCallInfoHarvester() {
		CallInfoHarvester harvestingExecutor = new CallInfoHarvester(frontend);
		
		T proxy = createForExecutor(harvestingExecutor, null, null);
		harvestingExecutor.setProxy(proxy);
		
		return harvestingExecutor;
	}
	
	private CallInfoHarvester<T> acquireHarvester() {
		if(callInfoHarvester == null) {
			callInfoHarvester = createCallInfoHarvester();
		}
		return callInfoHarvester;
	}
	
	/**
	 * <p>Extract call info from a resource method call. To extract call information,
	 * pass a lambda expression as an argument wich calls the method with the
	 * desired parameters. A call will look like this:</p>
	 * {@code clientFactory.infoOf(r -> r.myResourceMethod(arg1, arg2, ..)}
	 * @param callModel	a lambda expression calling the service method to examine
	 * @return call information
	 */
	public CallInfo infoOf(Consumer<T> callModel) {
		return acquireHarvester().harvest(callModel);
	}
	
}
