/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.spring;

import java.util.function.Supplier;
import org.bitbucket.restgap.backend.Backend;
import org.bitbucket.restgap.backend.PlanExecutor;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author upachler
 */
public final class RestTemplateBackend implements Backend {
	
	private final Supplier<RestTemplate> restTemplateSupplier;

	private RestTemplateBackend(Supplier<RestTemplate> restTemplateSupplier) {
		this.restTemplateSupplier = restTemplateSupplier;
	}

	/**
	 * Create a Backend that uses a new, plain {@link RestTemplate} instance
	 * for each new RestGap client that is created.
	 * @return a {@link Backend} based on a new {@code RestTemplate} instance
	 */
	public static Backend instance() {
		return instance(RestTemplate::new);
	}

	/**
	 * Create a Backend that uses uses the provided supplier to get 
	 * a {@link RestTemplate} instance for a new client. This is useful to 
	 * pass in pre-configured {@code RestTemplate} instances.
	 * @param restTemplateSupplier a {@code Supplier} supplying a 
	 *	{@code RestTemplate} to use for a new client. 
	 * @return a {@link Backend} that sources {@code RestTemplate}s from the 
	 *	given {@code Supplier}
	 */
	public static Backend instance(Supplier<RestTemplate> restTemplateSupplier) {
		return new RestTemplateBackend(restTemplateSupplier);
	}
	
	@Override
	public PlanExecutor createPlanExecutor() {
		return new RestTemplatePlanExecutor(restTemplateSupplier.get());
	}
	
}
