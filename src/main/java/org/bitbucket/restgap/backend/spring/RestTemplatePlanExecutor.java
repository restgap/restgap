/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.spring;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.bitbucket.restgap.backend.jaxrs.SimpleCallResult;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.client.RestCallContext;
import org.bitbucket.restgap.impl.AbstractPlanExecutor;
import org.bitbucket.restgap.impl.springmvc.SpringMVCUtil;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author count
 */
public class RestTemplatePlanExecutor extends AbstractPlanExecutor {
	
	private final RestTemplate restTemplate;

	public RestTemplatePlanExecutor(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	protected CompletableFuture<RestCallContext> performCall(RestCallContext ctx) {
		RestCall rc = ctx.getCall();
		HttpEntity entity;
		URI uri = buildUri(rc.getUri(), rc.getPathParams(), rc.getQueryParams());
		HttpMethod httpMethod = HttpMethod.resolve(rc.getHttpMethod());
		
		HttpHeaders headerMVM = new HttpHeaders();
		for(Map.Entry<String,List<Object>> e : rc.getHeaders().entrySet()) {
			List<String> values = new ArrayList<>(e.getValue().size());
			for(Object v : e.getValue()) {
				values.add(v.toString());
			}
			headerMVM.put(e.getKey(), values);
		}

		if(httpMethod != HttpMethod.POST || rc.getFormParams().isEmpty()) {
			entity = new HttpEntity(rc.getHttpBody(), headerMVM);
		} else {
			// special handling for POST with form params
			
			// first, we need to check if the Content-Type header is already set
			// to one for the form action headers. if it is, we go with that,
			// otherwise we'll overwrite the content type, as that can't
			// be correct otherwise
			if(headerMVM.getContentType() == null) {
				String[] consumes = ctx.getCall().getPlan().getConsumedMediaTypes();
				MediaType contentType = SpringMVCUtil.findMatchingFormMediaType(consumes);
				if(contentType == null) {
					contentType = MediaType.MULTIPART_FORM_DATA;
				}
				headerMVM.setContentType(contentType);
			}

			MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<>();
			for(Map.Entry<String, List<Object>> e : rc.getFormParams().entrySet()) {
				ArrayList<Object> vlist = new ArrayList<>(e.getValue().size());
				for(Object v : e.getValue()) {
					if (v instanceof File) {
						v = new FileSystemResource((File)v);
					}
					vlist.add(v);
				}
				formParams.put(e.getKey(), vlist);
			}
			entity = new HttpEntity(formParams, headerMVM);
		}
		try {
			ResponseEntity<?> response = restTemplate.exchange(uri, httpMethod, entity, toParameterizedTypeReference(rc.getResponseType()));
			ctx.setResult(new RestTemplateCallResult(response));
		} catch(HttpStatusCodeException hsce) {
			ctx.setResult(new SimpleCallResult(hsce.getStatusCode().value(), hsce.getResponseBodyAsByteArray(), hsce.getResponseHeaders()));
		}

		return CompletableFuture.completedFuture(ctx);
	}
	
	private ParameterizedTypeReference toParameterizedTypeReference(final Type type) {
		return new ParameterizedTypeReference<Object>() {
			@Override
			public Type getType() {
				if(type == void.class) {
					// Apparently, from Spring 5.0(+?), RestTemplate no longer
					// accepts void.class as response type - if passed in anyways,
					// it produces a broken Accept: header ('Accept: ' <-- empty string).
					// With null things work fine.
					return null;
				}
				return type;
			}
		};
	}
	
	private static URI buildUri(String baseUriString, Map<String,Object> pathParams, Map<String,List<Object>> queryParams) {
		UriComponentsBuilder b = UriComponentsBuilder.fromHttpUrl(baseUriString);
		
		for(Map.Entry<String, List<Object>> e : queryParams.entrySet()) {
			for(Object value : e.getValue()) {
				if(value == null) {
					// ingore null query parameters; there is no common way to
					// represent them (other than absence)
					continue;
				}
				String stringValue = value.toString();
				b.queryParam(e.getKey(), stringValue);
			}
		}
		URI uri = b.build().expand(pathParams).encode().toUri();
		return uri;
	}
	
}
