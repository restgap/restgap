/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.spring;

import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;
import org.bitbucket.restgap.backend.RestCallResult;

/**
 *
 * @author upachler
 */
class RestTemplateCallResult implements RestCallResult{
	private final ResponseEntity<?> response;

	RestTemplateCallResult(ResponseEntity<?> response){
		this.response = response;
	}

	@Override
	public int getStatusCode() {
		return response.getStatusCode().value();
	}

	@Override
	public Object getBody() {
		return response.getBody();
	}

	@Override
	public Map<String, List<String>> getHeaders() {
		return response.getHeaders();
	}

}
