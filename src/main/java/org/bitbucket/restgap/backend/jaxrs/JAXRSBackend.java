/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.jaxrs;

import java.util.function.Supplier;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import org.bitbucket.restgap.backend.Backend;
import org.bitbucket.restgap.backend.PlanExecutor;

/**
 * A {@link Backend} implementation that uses the JAX RS Client API to make
 * REST calls.
 * @author upachler
 */
public class JAXRSBackend implements Backend{
	
	Supplier<Client> jaxRsClientSupplier;

	private JAXRSBackend(Supplier<Client> jaxRsClientSupplier) {
		this.jaxRsClientSupplier = jaxRsClientSupplier;
	}
	
	/**
	 * Create a JAXRSBackend which uses the default {@link ClientBuilder} to
	 * create clients.
	 * @return a {@link Backend} using the JAX RS Client API
	 */
	public static JAXRSBackend instance() {
		return instance(ClientBuilder::newClient);
	}

	/**
	 * Create a JAXRSBackend which uses the provded {@code jaxrsClientSupplier}
	 * as a source for JAX RS {@link Client} instances, that are used to create
	 * actual RestGap clients. This is useful to use pre-configured  {@code Client}
	 * instances in RestGap.
	 * @param jaxrsClientSupplier	a {@link Supplier} providing JAX RS {@link Client} instances.
	 * @return a {@link Backend} using the JAX RS Client API
	 */
	public static JAXRSBackend instance(Supplier<Client> jaxrsClientSupplier) {
		return new JAXRSBackend(jaxrsClientSupplier);
	}
	
	@Override
	public PlanExecutor createPlanExecutor() {
		return new JAXRSPlanExecutor(jaxRsClientSupplier.get());
	}
	
}
