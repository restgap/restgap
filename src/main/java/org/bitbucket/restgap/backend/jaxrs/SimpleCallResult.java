/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.jaxrs;

import java.util.List;
import java.util.Map;
import org.bitbucket.restgap.backend.RestCallResult;

/**
 *
 * @author upachler
 */
public class SimpleCallResult implements RestCallResult {
	
	private final int statusCode;
	private final Object body;
	private final Map<String, List<String>> headers;

	public SimpleCallResult(int statusCode, Object body, Map<String, List<String>> headers) {
		this.statusCode = statusCode;
		this.body = body;
		this.headers = headers;
	}
	
	
	@Override
	public int getStatusCode() {
		return statusCode;
	}

	@Override
	public Object getBody() {
		return body;
	}

	@Override
	public Map<String, List<String>> getHeaders() {
		return headers;
	}
	
}
