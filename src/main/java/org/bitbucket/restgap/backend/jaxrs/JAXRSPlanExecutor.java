/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.jaxrs;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.MessageBodyReader;
import jakarta.ws.rs.ext.MessageBodyWriter;
import org.bitbucket.restgap.Plan;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.client.RestCallContext;
import org.bitbucket.restgap.impl.AbstractPlanExecutor;

/**
 *
 * @author upachler
 */
class JAXRSPlanExecutor extends AbstractPlanExecutor {

	private final Client client;
	
	List<MessageBodyReader> messageBodyReaders = new ArrayList<>();
	List<MessageBodyWriter> messageBodyWriters = new ArrayList<>();
	
	JAXRSPlanExecutor(Client clientImplementation) {
		this.client = clientImplementation;
		for(Object o : client.getConfiguration().getInstances()) {
			if(o instanceof MessageBodyReader) {
				MessageBodyReader r = (MessageBodyReader)o;
				messageBodyReaders.add(r);
			} else if (o instanceof MessageBodyWriter) {
				MessageBodyWriter w = (MessageBodyWriter)o;
				messageBodyWriters.add(w);
			}
		}
	}
	
	protected CompletableFuture<RestCallContext> performCall(RestCallContext ctx) {
		
		RestCall rc = ctx.getCall();
		
		WebTarget target = client.target(rc.getUri());
		Form form = null;
		
		// path parameters
		for(Map.Entry<String, Object> e : rc.getPathParams().entrySet()) {
			target = target.resolveTemplate(e.getKey(), e.getValue());
		}
		
		// query parameters
		if(rc.getQueryParams()!= null) {
			for(Map.Entry<String, List<Object>> e : rc.getQueryParams().entrySet()) {
				List<Object> l = e.getValue();
				if(l.size() == 1) {
					target = target.queryParam(e.getKey(), l.get(0));
				} else {
					target = target.queryParam(e.getKey(), l.toArray());
				}
			}
		}
		
		// form parameters
		Map<String, List<Object>> formParams = rc.getFormParams();
		if(formParams != null && !formParams.isEmpty()) {
			form = new Form();
			MultivaluedMap<String,String> mvm = new MultivaluedHashMap<>();
			for(Map.Entry<String, List<Object>> e : rc.getFormParams().entrySet()) {
				List<Object> l = e.getValue();
				for(Object value : l) {
					form.param(e.getKey(), value.toString());
				}
			}
		}
		
		// entity body
		Entity entity = null;
		if(rc.getHttpBody() != null) {
			MediaType mediaType = findMediaType(rc.getPlan(), rc.getHttpBody());
			entity = Entity.entity(rc.getHttpBody(), mediaType);
		} else if(form != null) {
			// FIXME: because we're only getting here if there is not HTTP body
			// passed as parameter, form parameters will not be included in
			// the request otherwise. There may be a way to change the Content-Type
			// to a multipart/* variant, but this will need further investigation.
			
			entity = Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		}
		
		// headers
		Invocation.Builder builder = target.request();
		if(rc.getHeaders() != null) {
			for(Map.Entry<String, List<Object>> e : rc.getHeaders().entrySet()) {
				for(Object v : e.getValue()) {
					builder.header(e.getKey(), v);
				}
			}
		}

		if(rc.getPlan().getProducedMediaTypes() != null) {
			builder = builder.accept(rc.getPlan().getProducedMediaTypes());
		}

		Invocation invocation = entity != null
		?	builder.build(rc.getHttpMethod(), entity)
		:	builder.build(rc.getHttpMethod());
		
		
		Response response = invocation.invoke();
		Type responseType = rc.getPlan().getResponseType();
		Object body = null;
		if(responseType instanceof Class && Response.class.isAssignableFrom((Class)responseType)) {
			body = response;
		} else if(response.getStatus()/100 == 2 && responseType != void.class && response.hasEntity()) {
			// we only attempt to read the entity on 2xx responses 
			body = response.readEntity(new GenericType(responseType));
		}
		
		ctx.setResult(new SimpleCallResult(response.getStatus(), body, toMVM(response.getHeaders())));
		return CompletableFuture.completedFuture(ctx);
	}
	
	private Map<String,List<String>> toMVM(MultivaluedMap<String,Object> jaxrsmvm){
		Map<String,List<String>> r = new HashMap<>();
		
		for(Map.Entry<String, List<Object>> e : jaxrsmvm.entrySet()) {
			List<String> l = new ArrayList<>();
			for(Object o : e.getValue()) {
				l.add(o.toString());
			}
			r.put(e.getKey(), l);
		}
		return r;
	}
	
	private MediaType findMediaType(Plan plan, Object body) {
		String[] mts = plan.getConsumedMediaTypes();
		if(mts.length>0) {
			return MediaType.valueOf(mts[0]);
		} else {
			return MediaType.WILDCARD_TYPE;
		}
	}
}
