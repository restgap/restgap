/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend;

import org.bitbucket.restgap.Plan;

/**
 * A RestGap backend is basically a factory for {@link PlanExecutor}s, which in
 * turn execute call {@link Plan}s created by a {@link Frontend}. The {@code PlanExecutor}s
 * created by the {@code Backend} use a specific client technology to perform REST calls.
 * @author upachler
 */
public interface Backend {
	public PlanExecutor createPlanExecutor();
	
}
