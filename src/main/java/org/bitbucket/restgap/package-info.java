/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * RestGap creates proxy clients for REST resource interfaces with 
 * annotations from popular frameworks like JAX RS or Spring MVC.
 * 
 * <p>Consider a REST service (a resource) on a base URI, like {@code http://acme.com/}, that
 * lives under {@code /hello} and takes a {@code name=..} query parameter. Calling
 * GET on {@code http://acme.com/hello?name=John} will return a hello message.</p>
 * 
 * <p>REST resources like this are modelled with annotated interfaces. For the 
 * resource interface of our example that we want to call...</p>
 * 
 * <pre>
 * &#64;Path("/hello")
 * public interface Prompter{
 *     &#64;GET
 *     &#64;Produces("text/plain")
 *     String sayHello(@literal &#64;QueryParam("name") String name);
 * }
 * </pre>
 * 
 * <p>...RestGap produces an <i>instance</i> that provides implementations of the
 * resource methods (like 'sayHello()') which actually perform a HTTP call
 * (<code>GET /hello?name=..</code> in this example).</p>
 * 
 * <p>To create clients, a {@link ClientFactory} must be created first</p>
 * 
 * Contains the RESTGap core factory classes {@link RESTTemplateSpringMVCFactory}
 * and {@link RESTTemplateJAXRSClientFactory}.
 */
package org.bitbucket.restgap;
