/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.util.ArrayList;
import java.util.List;
import org.bitbucket.restgap.client.CallHandler;
import org.bitbucket.restgap.client.RestGapClient;
import org.bitbucket.restgap.client.ResultHandler;

/**
 * Overriding RestGapClient to make handler accessors accessible to classes within 
 package.
 */
public class RestGapClientImpl extends RestGapClient{

	private final List<CallHandler> callHandlers = new ArrayList<>();
	private final List<ResultHandler> resultHandlers = new ArrayList<>();

	// These methods are only overridden to make them visible within the package
	// (yep, you need to override a protected method with public within
	// a package-private class for this...)
	
	public List<ResultHandler> getResultHandlers() {
		return resultHandlers;
	}

	public List<CallHandler> getCallHandlers() {
		return callHandlers;
	}

	@Override
	public void addCallHandler(CallHandler callHandler) {
		callHandlers.add(callHandler);
	}

	@Override
	public void addResultHandler(ResultHandler resultHandler) {
		resultHandlers.add(0, resultHandler);
	}
}
