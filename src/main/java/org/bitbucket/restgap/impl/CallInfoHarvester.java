/*
 * Copyright 2020 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import org.bitbucket.restgap.CallInfo;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.client.RestCallContext;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.impl.AbstractPlanExecutor;

/**
 *
 * @author upachler
 */
public class CallInfoHarvester<T> extends AbstractPlanExecutor {
	
	private final Frontend frontend;
	private T proxy;

	public CallInfoHarvester(Frontend frontend) {
		this.frontend = frontend;
	}

	public void setProxy(T proxy) {
		this.proxy = proxy;
	}

	public CallInfo harvest(Consumer<T> callModel) {
		try {
			// this will, via the proxy, call our performCall() implementation,
			// which in turn cancels the call. The resulting 
			// CancelledByRequestException contains the information we're after
			callModel.accept(proxy);
			throw new IllegalArgumentException("No resource method of the provided proxy was called in the given consumer");
		} catch (CancelledByRequestException cx) {
			RestCall rc = cx.getContext().getCall();
			return frontend.toCallInfo(rc);
		}
	}
	
	@Override
	protected CompletableFuture<RestCallContext> performCall(RestCallContext ctx) {
		ctx.abort();
		return CompletableFuture.completedFuture(ctx);
	}
	
}
