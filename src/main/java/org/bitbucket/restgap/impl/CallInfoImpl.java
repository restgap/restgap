/*
 * Copyright 2020 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.net.URI;
import org.bitbucket.restgap.CallInfo;
import org.bitbucket.restgap.frontend.Frontend;

/**
 * Default implementation of CallInfo, to use in {@link Frontend} implementations
 * @author upachler
 */
public class CallInfoImpl implements CallInfo{
	private final URI uri;
	private final String method;

	public CallInfoImpl(URI uri, String method) {
		this.uri = uri;
		this.method = method;
	}

	@Override
	public URI uri() {
		return uri;
	}

	@Override
	public String method() {
		return method;
	}
}
