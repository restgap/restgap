/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Consumer;
import org.bitbucket.restgap.ParamKind;
import org.bitbucket.restgap.ParameterDefinition;
import org.bitbucket.restgap.Plan;
import org.bitbucket.restgap.backend.PlanExecutor;
import org.bitbucket.restgap.client.CallHandler;
import org.bitbucket.restgap.client.RestCallContext;
import org.bitbucket.restgap.client.ResultHandler;

/**
 *
 * @author upachler
 */
public abstract class AbstractPlanExecutor implements PlanExecutor {

	
	public static class CancelledByRequestException extends CancellationException {

		private final RestCallContext context;

		public CancelledByRequestException(RestCallContext ctx) {
			context = ctx;
		}

		public RestCallContext getContext() {
			return context;
		}
	}
	
	@Override
	public Object execute(Plan plan, RestGapClientImpl c, Object[] args) {
		try {
			return prepare(plan, c, args)
				.join();
		} catch(CompletionException cx) {
			if(cx.getCause() instanceof RuntimeException) {
				throw (RuntimeException)cx.getCause();
			} else {
				throw cx;
			}
		}
	}
	
	static private <K,V, E extends V> Map<K,List<V>> addMVMValueImpl(Map<K,List<V>> mvm, K key, Consumer<List<V>> c) {
		List<V> l = mvm.get(key);;
		if(l == null) {
			l = new ArrayList<>();
			mvm.put(key, l);
		}
		c.accept(l);
		return mvm;
	}
	
	static private <K,V> Map<K,List<V>> addMVMValue(Map<K,List<V>> mvm, K key, V value) {
		return addMVMValueImpl(mvm, key, l -> l.add(value));
	}
	
	static private <K,V, E extends V> Map<K,List<V>> addMVMValues(Map<K,List<V>> mvm, K key, Collection<E> value) {
		return addMVMValueImpl(mvm, key, l -> l.addAll(value));
	}
	
	private void copyToFile(InputStream in, File file) throws IOException {
		byte[] buffer = new byte[16384];
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
			for(int n=in.read(buffer); n!=-1; n=in.read(buffer)) {
				fos.write(buffer, 0, n);
			}
		} finally {
			if(fos != null) {
				fos.close();
			}
			in.close();
		}
		
	}
	
	private Object screen(Object arg, List<Runnable> cleanupHandlers) {
		//
		// Ensure that parameters are idempotent (i.e. that the request can
		// be repeated multiple times)
		//
		// Input paramters which get modified during the execution of a request
		// present a problem in scenarios where requests get repeated (for 
		// instance, when a stale access token gets rejected by the server, and
		// the client attempts to re-issue the request with a new token.
		// 
		// The prime example is an input stream: Once read during the first
		// request, most streams cannot be rewound. Even worse, some frameworks
		// actually close the stream after reading the data. All this makes 
		// the InputStream parameter a single-use object.
		//
		// The method below converts in InputStream into a java.io.File, which
		// Both JAX RS and Spring MVC accept as input. The File gets cleaned up
		// afte the request has finally completed.
		//
		if(arg instanceof InputStream) {
			try {
				File temp = File.createTempFile("restgap.", ".tmp");
				copyToFile((InputStream)arg, temp);
				cleanupHandlers.add(new Runnable() {
					public void run() {
						temp.delete();
					}
				});
				return temp;
			} catch (IOException ex) {
				throw new RuntimeException(ex);
			}
		} else {
			return arg;
		}
	}
	
	private CompletableFuture<Object> prepare(Plan plan, RestGapClientImpl c, Object[] args) {

		RestCallContextImpl context = new RestCallContextImpl();
		
		Map<String,List<Object>> queryParams = new HashMap<>();
		Map<String,List<Object>> formParams = new HashMap<>();
		Map<String, Object> pathParams = new HashMap<>();
		ArrayList<Runnable> cleanupHandlers = new ArrayList<>(0);
		Object body = null;
		Map<String,List<Object>> headers = new HashMap<>();

		for (ParameterDefinition pdef : plan.getParameterDefinitions()) {
			Object arg = args[pdef.getArgIndex()];
			ParamKind kind = pdef.getKind();
			String name = pdef.getName();
			if(arg == null) {
				if(kind==ParamKind.PATH) {
					throw new NullPointerException("path template parameter '"+name+"' may not be null");
				} else if(kind!=ParamKind.ENTITY) {
					// don't process null parameters of kinds other than ENTITY
					continue;
				}
			}
			
			switch (kind) {
				case ENTITY:
					body = screen(arg, cleanupHandlers);
					break;
					
				case PATH:
					pathParams.put(name, arg);
					break;
					
				case QUERY:
					queryParams = addMVMValue(queryParams, name, arg);
					break;
				case QUERY_COLLECTION:
					queryParams = addMVMValues(queryParams, name, ((Collection)arg));
					break;
				case QUERY_MAP:
					for(Map.Entry e : ((Map<String,Object>)arg).entrySet()) {
						queryParams = addMVMValue(queryParams, e.getKey().toString(), e.getValue());
					}
					break;
				case QUERY_MULTIMAP:
					for(Map.Entry<String, List<String>> e : (((Map<String, List<String>>)arg).entrySet())) {
						queryParams = addMVMValues(queryParams, e.getKey(), e.getValue());
					}
					break;
					
				case FORM:
					formParams = addMVMValue(formParams, name, screen(arg, cleanupHandlers));
					break;
					
				case HEADER:
					headers = addMVMValue(headers, name, arg.toString());
					break;
			}
		}
		

		
		RestCallImpl rc = new RestCallImpl(plan, plan.getPathTemplate(), plan.getHttpMethod(), headers, body, plan.getResponseType(), pathParams, queryParams, formParams, cleanupHandlers);
		context.setCall(rc);
		CompletableFuture<RestCallContext> cf = CompletableFuture.completedFuture(context);
		
		return prepareCall(c, cf)
			.thenCompose(this::checkCancellation)
			.thenApply(ctx -> ctx.getCall().getPlan().getResponseConverter().convert(ctx.getResult(), ctx.getCall().getPlan()));
	}
	
	private CompletableFuture<RestCallContext> checkCancellation(RestCallContext ctx) {
		if(ctx.isReissueRequested() || ctx.isAbortRequested()) {
			throw new CancelledByRequestException(ctx);
		} else {
			return CompletableFuture.completedFuture(ctx);
		}
	}
	
	private CompletableFuture<RestCallContext> prepareCall(RestGapClientImpl c, CompletableFuture<RestCallContext> cf) {
		
		List<CallHandler> callHandlers = c.getCallHandlers();
		if(callHandlers != null) {
			for(CallHandler ch : callHandlers) {
				cf = cf.thenCompose(ch::handleCall);
			}
		}
		
		// schedule REST call
		cf = cf.thenCompose(this::performCall);
		
		// schedule call handlers. Note that each of these handlers may abort
		// processing the call result by calling RestCallContext.abort() or
		// RestCallContext.reissue();
		List<ResultHandler> resultHandlers = c.getResultHandlers();
		if(resultHandlers != null) {
			for(ResultHandler rh : resultHandlers) {
				cf = cf
					// schedule result handler
					.thenCompose(rh::handleResult)
					// compose function checking if reissue was requested, and cancels
					// this and all following stages
					.thenCompose(this::checkCancellation);
			}
		}
		cf = cf
			.exceptionally(t -> {
				if(t.getCause() instanceof CancelledByRequestException) {
					return ((CancelledByRequestException)t.getCause()).getContext();
				} else {
					throw new CompletionException(t);
				}
			})
			.thenCompose(ctx -> {
				if(ctx.isReissueRequested()) {
					ctx.clearAlternateFlow();
					return prepareCall(c, CompletableFuture.completedFuture(ctx));
				} else {
					((RestCallImpl)ctx.getCall()).cleanup();
					return CompletableFuture.completedFuture(ctx);
				}
			});
		
		return cf;
	}
		
	protected abstract CompletableFuture<RestCallContext> performCall(RestCallContext ctx);

}
