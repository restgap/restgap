/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.util.HashMap;
import java.util.Map;
import org.bitbucket.restgap.backend.RestCallResult;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.client.RestCallContext;

/**
 *
 * @author upachler
 */
public class RestCallContextImpl implements RestCallContext{

	public enum Action {
		PROCEED,
		REISSUE,
		ABORT
	}
	
	private Map<String,Object> properties = new HashMap<>();
	private Action action = Action.PROCEED;
	private RestCall call;
	private RestCallResult result;

	public RestCall getCall() {
		return call;
	}

	public void setCall(RestCall call) {
		this.call = call;
	}

	public RestCallResult getResult() {
		return result;
	}

	public void setResult(RestCallResult result) {
		this.result = result;
	}

	
	@Override
	public Object getProperty(String name) {
		return properties.get(name);
	}
	
	@Override
	public void setProperty(String name, Object value) {
		properties.put(name, value);
	}

	@Override
	public void reissue() {
		action = Action.REISSUE;
	}

	@Override
	public void abort() {
		action = Action.ABORT;
	}

	@Override
	public boolean isReissueRequested() {
		return action==Action.REISSUE;
	}

	@Override
	public boolean isAbortRequested() {
		return action==Action.ABORT;
	}

	@Override
	public void clearAlternateFlow() {
		action = Action.PROCEED;
	}
}
