/*
 * Copyright 2019 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl.springmvc;

import org.springframework.http.MediaType;

/**
 *
 * @author upachler
 */
public class SpringMVCUtil {

	public static MediaType findMatchingFormMediaType(String[] consumedMtNames) {
		for (String consumesString : consumedMtNames) {
			MediaType consumesmt = MediaType.valueOf(consumesString);
			if (MediaType.APPLICATION_FORM_URLENCODED.includes(consumesmt)) {
				return MediaType.APPLICATION_FORM_URLENCODED;
			} else if (MediaType.MULTIPART_FORM_DATA.includes(consumesmt)) {
				return MediaType.MULTIPART_FORM_DATA;
			}
		}
		return null;
	}
	
}
