/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.bitbucket.restgap.client;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * When registered via {@link RestGapClient#addCallHandler(CallHandler)}, a call handler can change
 * the call parameters that are passed to the backend before the call is
 * made.
 * @author count
 */
public interface CallHandler {

	/**
	* When registered via {@link RestGapClient#addCallHandler(CallHandler)}, a call handler's 
	* {@link #handleCall(RestCallContext)} method is invoked right before HTTP request is made.
	* The call handler can change the
	* actual call parameters, e.g. to add authentication information. The method
	* needs to return a {@linkplain CompletionStage} instance. If the action
	* performed by the call handler completes synchronously (i.e. when this method
	* terminates), a completed stage should be returned, typically with 
	* {@link CompletableFuture#completedFuture }. However, if the action is performed
	* asynchronously, use the other {@linkplain CompletableFuture}'s methods
	* to compose an incomplete completion stage.
	* @param	ctx	Context object representing the current RESTgap client 
	*	invocation
	* @return the CompletionStage holding the state of completion for the
	*	RestCallContext.
	*/
	CompletionStage<RestCallContext> handleCall(RestCallContext ctx);
	
}
