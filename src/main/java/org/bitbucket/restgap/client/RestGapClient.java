/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.client;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import org.bitbucket.restgap.RESTTemplateJAXRSClientFactory;
import org.bitbucket.restgap.RESTTemplateSpringMVCFactory;
import org.springframework.web.client.RestTemplate;

/**
 * REST clients created by the {@link RESTTemplateJAXRSClientFactory#create(RestTemplate, String, Class)} and
 * {@link RESTTemplateSpringMVCFactory#create(RestTemplate, String, Class) } methods allow access to the
 * underlying client infrastructure via this interface. The <code>RestGapClient</code>
 * instance can be obtained via the {@link #getClient(Object)} method.
 * @author count
 */
// NOTE: This is an abstract class rather than an interface so that it can
// have a static method to actually obtain a RestGapClient instance from a proxy
// REST client instance. In pure Java 8 this would be an interface with a 
// static method, but because we want this to be available on earlier Java
// versions, all we have left is to use a public abstract class.
public abstract class RestGapClient {
	
	/**
	 * Provides the RestGapClient for a particular REST client instance created by 
 one of the RestGap factories.
	 * @param o	The REST client instance.
	 * @return	The RestGapClient instance that belongs to the REST client
	 * @throws	ClassCastException	Will be thrown if the provided object was
	 *	not created by the RestGap factories
	 */
	public static RestGapClient getClient(Object o) {
		return (RestGapClient)Proxy.getInvocationHandler(o);
	}
	
	public abstract void addCallHandler(CallHandler callHandler);
	
	public abstract void addResultHandler(ResultHandler resultHandler);

	
}
