/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

/**
 *
 * @author count
 */
public class ParameterDefinition {
	
	final ParamKind kind;
	final String name;
	final int argIndex;

	public ParameterDefinition(ParamKind kind, String name, int argIndex) {
		this.kind = kind;
		this.name = name;
		this.argIndex = argIndex;
	}

	public ParamKind getKind() {
		return kind;
	}

	public String getName() {
		return name;
	}

	public int getArgIndex() {
		return argIndex;
	}
	
}
