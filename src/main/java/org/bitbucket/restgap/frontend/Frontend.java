/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend;

import java.lang.reflect.Method;
import java.util.Map;
import org.bitbucket.restgap.CallInfo;
import org.bitbucket.restgap.Plan;
import org.bitbucket.restgap.backend.Backend;
import org.bitbucket.restgap.client.RestCall;

/**
 * Capsules a specific frontend technology (like JAX-RS or Spring MVC). A 
 * {@linkplain Frontend} implementation is responsible for
 * <ul>
 * <li>Producing {@link Plan}s by parsing annotations on resource interfaces</li>
 * <li>Converting {@link RestCall}s to {@link CallInfo}s by leveraging the 
 * technology's URI building facilities</li>
 * </ul>
 * @author upachler
 */
public interface Frontend {
	/**
	 * Create call plans by analysing the provided resource interface. The
	 * Plans are used by a {@link Backend} (via the client instance) to make
	 * the actual REST call. The returned map associates method signature strings
	 * (as obtained from {@link Method#getGenericSignature() }) with a 
	 * {@link Plan} instance that defines on how to translate a call to this
	 * method into a REST call.
	 * @param clazz	the resource interface to analyse. Must be a Java interface
	 * @param baseUri	the base URI of for all resources in the interface
	 * @return mapping from method signatures to plans
	 */
	Map<String,Plan> createPlans(Class<?> clazz, String baseUri);
	
	/**
	 * Convert a {@link RestCall} to a {@link CallInfo} instance. To perform
	 * such a conversion, the {@linkplain Frontend} uses the client technology's
	 * URI building facilities. 
	 * @param rc	RestCall to transform
	 * @return call information
	 */
	CallInfo toCallInfo(RestCall rc);
}
