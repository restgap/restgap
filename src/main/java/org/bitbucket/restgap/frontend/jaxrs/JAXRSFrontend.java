/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend.jaxrs;

import org.bitbucket.restgap.impl.PlanUtils;
import org.bitbucket.restgap.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.CookieParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.OPTIONS;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HEAD;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.UriBuilder;
import org.bitbucket.restgap.backend.RestCallResult;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.frontend.ResponseConverter;
import org.bitbucket.restgap.impl.CallInfoImpl;
import org.springframework.http.HttpMethod;

/**
 * This factory creates a map from method signature strings to {@link Plan}s.
 * It analyses a given class and constructs {@link Plan} instances for each
 * method that carries JAX RS annotations.
 */
public final class JAXRSFrontend implements Frontend {

	static final Frontend INSTANCE = new JAXRSFrontend();

	static final String[] WILDCARD_MEDIA_TYPES = { MediaType.WILDCARD };

	public static Frontend instance() {
		return INSTANCE;
	}

	@Override
	public Map<String, Plan> createPlans(Class<?> clazz, String baseUri) {


		Path path = clazz.getAnnotation(Path.class);

		String baseResourcePath = appendPath(baseUri, path);

		Map<String,Plan> plans = new HashMap<>();
		for(Method m : clazz.getDeclaredMethods()) {

			Plan plan = mkPlan(baseResourcePath, m);
			plans.put(m.toGenericString(), plan);
		}
		return plans;
	}

	@Override
	public CallInfo toCallInfo(RestCall rc) {
		UriBuilder b = UriBuilder.fromUri(rc.getUri());

		// query parameters
		for(Map.Entry<String, List<Object>> e : rc.getQueryParams().entrySet()) {
			String name = e.getKey();
			List<Object> values = e.getValue();
			b.queryParam(name, values.toArray());
		}

		// build with path parameters
		URI uri = b.buildFromMap(rc.getPathParams());
		String method = rc.getHttpMethod();

		return new CallInfoImpl(uri, method);
	}


	private static String appendPath(String baseUri, Path path) {
		return PlanUtils.appendPath(baseUri, path!=null ? path.value() : null);
	}

	private static String mapHttpMethod(Method m) {

        jakarta.ws.rs.HttpMethod httpMethod = m.getAnnotation(jakarta.ws.rs.HttpMethod.class);
			if(httpMethod == null) {
				for(Annotation a : m.getAnnotations()) {
					httpMethod = a.annotationType().getAnnotation(jakarta.ws.rs.HttpMethod.class);
					if(httpMethod != null) {
						break;
					}
				}
			}
			if(httpMethod == null) {
				return null;
			}

			return httpMethod.value();
	}



	private static Plan mkPlan(String basePath, Method m) {
		final String httpMethod = mapHttpMethod(m);

		Path path = m.getAnnotation(Path.class);
		final String pathTemplate = appendPath(basePath, path);

		Produces produces = m.getAnnotation(Produces.class);
		String[] producedMediaTypes = produces != null
			?	produces.value()
			:	WILDCARD_MEDIA_TYPES;

		Consumes consumes = m.getAnnotation(Consumes.class);
		String[] consumedMediaTypes = consumes != null
			?	consumes.value()
			:	WILDCARD_MEDIA_TYPES;

		List<ParameterDefinition> pdefs = new ArrayList<>();

		Function<RestCallResult,Object> converterFunction;
		final Type responseType;
		Type returnType = m.getGenericReturnType();
		if(Void.class == returnType) {
			responseType = null;
			converterFunction = x -> null;
		} else if(returnType instanceof Class || returnType instanceof ParameterizedType){
			responseType = returnType;
			converterFunction = RestCallResult::getBody;
		} else {
			throw new UnsupportedOperationException(String.format("Unsupported return type %s in method %s", returnType.toString(), m.getName()));
		}

		Annotation[][] annotations = m.getParameterAnnotations();
		Class[] types = m.getParameterTypes();
		for(int i=0; i<annotations.length; ++i) {
			PathParam pathParam = PlanUtils.getAnnotation(annotations[i], PathParam.class);
			QueryParam queryParam = PlanUtils.getAnnotation(annotations[i], QueryParam.class);
			FormParam formParam = PlanUtils.getAnnotation(annotations[i], FormParam.class);
			HeaderParam headerParam = PlanUtils.getAnnotation(annotations[i], HeaderParam.class);
			CookieParam cookieParam = PlanUtils.getAnnotation(annotations[i], CookieParam.class);
			MatrixParam matrixParam = PlanUtils.getAnnotation(annotations[i], MatrixParam.class);

			if(pathParam != null) {
				pdefs.add(new ParameterDefinition(ParamKind.PATH, pathParam.value(), i));
			} else if(queryParam!=null) {
				ParamKind paramKind;
				if(List.class.isAssignableFrom(types[i]) || Set.class.isAssignableFrom(types[i])) {
					paramKind = ParamKind.QUERY_COLLECTION;
				} else {
					paramKind = ParamKind.QUERY;
				}
				pdefs.add(new ParameterDefinition(paramKind, queryParam.value(), i));
			} else if(formParam!=null) {
				pdefs.add(new ParameterDefinition(ParamKind.FORM, formParam.value(), i));
			} else if(headerParam!=null) {
				pdefs.add(new ParameterDefinition(ParamKind.HEADER, headerParam.value().toLowerCase(), i));
			} else if(cookieParam!=null || matrixParam!=null) {
				throw new UnsupportedOperationException("CookieParam and MatrixParam are not yet supported");
			} else {
				pdefs.add(new ParameterDefinition(ParamKind.ENTITY, null, i));
			}
		}

		final ParameterDefinition[] parameterDefinitions = pdefs.toArray(new ParameterDefinition[pdefs.size()]);

		ResponseConverter converter = new JAXRSResponseConverter(converterFunction);
		return new Plan(httpMethod, pathTemplate, responseType, parameterDefinitions, converter, producedMediaTypes, consumedMediaTypes);
	}


}
