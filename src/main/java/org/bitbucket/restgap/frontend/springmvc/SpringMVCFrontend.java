/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend.springmvc;

import org.bitbucket.restgap.*;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import static org.bitbucket.restgap.impl.PlanUtils.appendPath;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.frontend.ResponseConverter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.bitbucket.restgap.backend.RestCallResult;
import org.bitbucket.restgap.client.RestCall;
import org.bitbucket.restgap.impl.CallInfoImpl;
import org.bitbucket.restgap.impl.springmvc.SpringMVCUtil;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Creates a map from method signature Strings to {@link Plan}s. The factory
 * will analyse Spring MVC annotations on a given interface' class object
 * to generate the plans.
 * 
 * The factory will require you to use controller interfaces in Spring. So 
 * you'll have an interface like this:
 <pre>
 
 * &#64;Controller
 public interface BankServer {
    &#64;RequestMapping(path = "bank/{accountNumber}/balance", method = RequestMethod.GET, produces = "application/json")
    &#64;ResponseBody
	public float balance(&#64;PathParameter("account") String accountNumber);
 }
 * 
</pre>
 * 
 * ..with a server-side implementation like that:
 * 
 <pre>
 
 &#64;Controller
 public class BankServerImpl implements BankServer {
    &#64;Override
	public float balance(String accountNumber) {
	    // return whatever is in this account...
		...
	}
}

</pre>
 * 
 * 
 * <h2>Supported Features</h2>
 * 
 * This frontend class is intended for the REST aspect
 * of Spring MVC; the actual view aspect is not supported. Therefore, only
 * a small selection of {@link RequestMapping}'s parameter and return type
 * features are supported. These are:
 * 
 * <h3>Supported Parameter Features</h3>
 * All method parameters must be annotated with one of the following annotations:
 * <ul>
 * <li>{@link PathVariable} - for variable path segment like <code>/foo/{variable}/bar</code></li>
 * <li>{@link RequestParam} - to support URL query params like <code>?param=value&amp;param2=value2</code></li>
 * </ul>
 * We will support these in the future (currently unsupported):
 * <ul>
 * <li>{@link MatrixVariable}</li>
 * <li>{@link RequestHeader}</li>
 * <li>{@link RequestBody}</li>
 * <li>{@link RequestPart}</li>
 * </ul>
 * Other parameter features or parameters without annotations are not supported.
 * 
 * <h3>Supported Return Types</h3>
 * With return types, there is the choice between specifying the {@link ResponseBody}
 * annotation and/or returning a {@link ResponseEntity}:
 * <ul>
 * <li>{@link HttpEntity} (or {@link ResponseEntity}) to return the response directly</li>
 * <li>{@link ResponseBody} annotation to convert any return types to a HTTP response body using a message converter configured in the {@linkplain RestTemplate}</li>
 * </ul>
 * Most other features are unsupported because they're not REST related. Asynchronous
 * results may be suppored at a later stage.
 * 
 * 
 * @author uwe_pachler
 */
public final class SpringMVCFrontend implements Frontend {
	
	static final Frontend INSTANCE = new SpringMVCFrontend();
	static final String[] WILDCARD_MEDIA_TYPES = {"*/*"};
	
	public static final Frontend instance() {
		return INSTANCE;
	}
	
	@Override
	public Map<String, Plan> createPlans(Class<?> clazz, String uri) {
		
		
		RequestMapping rm = clazz.getAnnotation(RequestMapping.class);
		
		String baseUri = appendPath(uri, getPath(rm));
		
		Map<String,Plan> plans = new HashMap<>();
		for(Method m : clazz.getDeclaredMethods()) {
			plans.put(m.toGenericString(), mkPlan(m, baseUri, rm));
		}
		return plans;
	}

	@Override
	public CallInfo toCallInfo(RestCall rc) {
		UriComponentsBuilder b = UriComponentsBuilder.fromUriString(rc.getUri());
		for(Map.Entry<String, List<Object>> e : rc.getQueryParams().entrySet()) {
			String name = e.getKey();
			List<Object> values = e.getValue();
			b.queryParam(name, values.toArray());
		}
		URI uri = b.buildAndExpand(rc.getPathParams())
			.toUri();
		String method = rc.getHttpMethod();
		return new CallInfoImpl(uri, method);
	}
	
	
	private static String getPath(RequestMapping rm) {
		if(rm==null) {
			return null;
		}
		List<String> paths = new ArrayList<>();
		paths.addAll(Arrays.asList(rm.value()));
		paths.addAll(Arrays.asList(rm.path()));
		if(!paths.isEmpty()) {
			return paths.get(0);
		} else {
			return null;
		}
	}
	
	private static Set<RequestMethod> requestMethodSet(RequestMethod[] methods) {
		Set<RequestMethod> rmset = EnumSet.noneOf(RequestMethod.class);
		rmset.addAll(Arrays.asList(methods));
		return rmset;
	}
	
	private static HttpMethod extractHttpMethod(RequestMapping mapping, RequestMapping parentMapping, boolean hasRequestBody) {
		// extract effective request method filter set (a server method would
		// only be called if the request uses one of these  methods)
		Set<RequestMethod> mappingSet;
		RequestMethod[] parentMethods = parentMapping!=null ? parentMapping.method() : new RequestMethod[0];
		if(mapping.method().length > 0) {
			mappingSet = requestMethodSet(mapping.method());
			if(parentMethods.length > 0) {
				Set<RequestMethod> parentMappingSet = requestMethodSet(parentMethods);
				mappingSet.retainAll(parentMappingSet);
			}
		} else if(parentMethods.length > 0) {
			mappingSet = requestMethodSet(parentMethods);
		} else {
			mappingSet = EnumSet.allOf(RequestMethod.class);
		}
		
		// now that we have the set of HTTP methods, we need to choose the
		// one we're going to use for calling (hey, we're building a client, 
		// not server :)
		if(hasRequestBody) {
			if(mappingSet.contains(RequestMethod.PUT)) {
				return HttpMethod.PUT;
			} else if(mappingSet.contains(RequestMethod.POST)) {
				return HttpMethod.POST;
			}
		} else {
			if(mappingSet.contains(RequestMethod.GET)) {
				return HttpMethod.GET;
			} else if(mappingSet.contains(RequestMethod.POST)) {
				return HttpMethod.POST;
			}
		}
		
		// otherwise we take the first we get
		return HttpMethod.resolve(mappingSet.iterator().next().name());
	}
	
	
	private static Plan mkPlan(Method m, String baseUri, RequestMapping parentMapping) {
		RequestMapping rm = m.getAnnotation(RequestMapping.class);
		
		baseUri = appendPath(baseUri, getPath(rm));
		
		ResponseBody responseBody = m.getAnnotation(ResponseBody.class);
		
		List<ParameterDefinition> params = new ArrayList<>(m.getParameterCount());
		Parameter[] parameters = m.getParameters();
		for(int argIndex=0; argIndex<parameters.length; ++argIndex) {
			Parameter p = parameters[argIndex];
			PathVariable pathVariable = p.getAnnotation(PathVariable.class);
			RequestParam requestParam = p.getAnnotation(RequestParam.class);
			MatrixVariable matrixVariable = p.getAnnotation(MatrixVariable.class);
			RequestHeader requestHeader = p.getAnnotation(RequestHeader.class);
			RequestBody requestBody = p.getAnnotation(RequestBody.class);
			RequestPart requestPart = p.getAnnotation(RequestPart.class);
			// FIXME: HeaderParam is not yet evaluated here..
			
			String name = "";
			ParamKind kind;
			if(pathVariable != null) {
				name = pathVariable.value();
				kind = ParamKind.PATH;
			} else if(requestBody != null) {
				kind = ParamKind.ENTITY;
			} else if(requestParam != null) {
				name = requestParam.name();
				if(name.isEmpty()) {
					name = requestParam.value();
				}

				if (hasMediaTypeWwwOrMultipartForm(rm)) {
					kind = ParamKind.FORM;
				} else if(Map.class.isAssignableFrom(p.getType()) || MultiValueMap.class.isAssignableFrom(p.getType())) {
					ParameterizedType type = (ParameterizedType)m.getGenericParameterTypes()[argIndex];
					Type[] typeArgs = type.getActualTypeArguments();
					if(typeArgs[0].equals(String.class) && typeArgs[1].equals(String.class)) {
						kind = ParamKind.QUERY_MAP;
					} else {
						kind = ParamKind.QUERY;
					}
				} else if(Collection.class.isAssignableFrom(p.getType())) {
					kind = ParamKind.QUERY_COLLECTION;
				} else {
					kind = ParamKind.QUERY;
				}
			} else {
				throw new IllegalArgumentException(String.format("parameter %d in method %s cannot be mapped", argIndex, m.getName()));
			}
			
			params.add(new ParameterDefinition(kind, name, argIndex));
		}
		
		HttpMethod method = extractHttpMethod(rm, parentMapping, responseBody!=null);
		
		Type responseType;
		
		Function<RestCallResult, Object> converterFunction;
		if(HttpEntity.class.isAssignableFrom(m.getReturnType())) {
			Type gtype = m.getGenericReturnType();
			if(gtype instanceof ParameterizedType) {
				responseType = ((ParameterizedType) gtype).getActualTypeArguments()[0];
			} else {
				responseType = Map.class;
			}
			converterFunction = (RestCallResult r) -> {
				MultiValueMap<String,String> mvm = new LinkedMultiValueMap<>(r.getHeaders().size());
				for(Map.Entry<String,List<String>> e : r.getHeaders().entrySet()) {
					for(String value : e.getValue()) {
						mvm.add(e.getKey(), value);
					}
				}
				return new ResponseEntity(r.getBody(), mvm, HttpStatus.valueOf(r.getStatusCode()));
			};
		} else if(responseBody!=null) {
			responseType = m.getGenericReturnType();
			converterFunction = RestCallResult::getBody;
		} else {
			// FIXME: not returning anything isn't quite right, there are more cases to consider!
			responseType = null;
			converterFunction = r -> null;
		}
		
		
		ResponseConverter responseConverter = new SpringMVCResponseConverter(converterFunction);
		return new Plan(method.name(), baseUri, responseType, params.toArray(new ParameterDefinition[params.size()]), responseConverter, convertMediaTypes(rm.produces()), convertMediaTypes(rm.consumes()));
	}

	private static boolean hasMediaTypeWwwOrMultipartForm(RequestMapping rm) {
		if (rm == null || rm.consumes() == null || rm.consumes().length <= 0) {
			return false;
		}
		return null != SpringMVCUtil.findMatchingFormMediaType(rm.consumes());
	}
	
	private static boolean isFilter(String f) {
		return f.trim().startsWith("!");
	}
	
	private static String[] convertMediaTypes(String[] mediaTypes) {
		if(mediaTypes.length == 0) {
			return WILDCARD_MEDIA_TYPES;
		} else {
			boolean hasNegations = false;
			for(String m : mediaTypes) {
				if(isFilter(m)) {
					hasNegations = true;
					break;
				}
			}
			
			if(!hasNegations) {
				return mediaTypes;
			} else {
				ArrayList<String> mediaTypeList = new ArrayList<>();
				for(String m : mediaTypes) {
					if(!isFilter(m)) {
						mediaTypeList.add(m);
					}
				}
				
				if(mediaTypeList.isEmpty()) {
					return WILDCARD_MEDIA_TYPES;
				} else {
					return mediaTypeList.toArray(new String[mediaTypeList.size()]);
				}
			}
				
		}
	}
}
