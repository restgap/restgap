/*
 * Copyright 2020 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.net.URI;

/**
 * Contains information about the service call made when calling
 * {@link ClientFactory#infoOf}.
 */
public interface CallInfo {

	/**
	 * The HTTP method used in the call, like "GET", "POST", etc..
	 * @return the HTTP method
	 */
	public String method();

	/**
	 * The URI used in the call. Can be relative or absolute, depending on
	 * the client configuration that was used to produce the harvester that
	 * created this instance.
	 * @return the URI that'd be called
	 */
	public URI uri();
}
